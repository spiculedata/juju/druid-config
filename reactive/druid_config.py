import os
from subprocess import run as sub_run
from charms.reactive import when, when_any, when_not, set_flag, clear_flag, endpoint_from_flag
from charms.templating.jinja2 import render
from charmhelpers.core.hookenv import resource_get, status_set, log
from charmhelpers.core import hookenv
from charms.reactive.helpers import data_changed
from subprocess import check_call


@when_not('druid-config.installed')
def install_druid_config():
    # Do your setup here.
    #
    # If your charm has other dependencies before it can install,
    # add those as @when() clauses above., or as additional @when()
    # decorated handlers below
    #
    # See the following for information about reactive charms:
    #
    #  * https://jujucharms.com/docs/devel/developer-getting-started
    #  * https://github.com/juju-solutions/layer-basic#overview
    #
    set_flag('druid-config.installed')


@when('zookeeper.joined')
@when_not('zookeeper.ready')
def wait_for_zookeeper(zookeeper):
    """
         We always run in Distributed mode, so wait for Zookeeper to become available.
    """
    status_set('waiting', 'Waiting for Zookeeper to become available')


@when_not('zookeeper.joined')
def wait_for_zkjoin():
    """
        Wait for Zookeeper
    """
    status_set('waiting', 'Waiting for Zookeeper to become joined')


@when_not('mysql.available')
def wait_for_mysqljoin():
    """
        Wait for MySQL
    """
    status_set('waiting', 'Waiting for MySQL to become joined')


@when('druid-config.installed', 'zookeeper.ready', 'mysql.available')
def druid_config_ready():
    set_flag('druid-config.ready')
    status_set('waiting', 'Waiting for Druid charms to connect...')


@when('druid-config.installed', 'zookeeper.ready', 'hadoop.hdfs.ready', 'endpoint.config.joined')
def send_hdfs_files(hadoop, zookeeper, endpoint):
    """
        Writes the Hadoop XML files once Hadoop is joined. This method should be triggered whenever a new Druid charm
        is connected.
    """
    status_set('maintenance', 'Preparing Hadoop XML Files')
    files = []

    with open('/etc/hadoop/conf.empty/core-site.xml', 'r') as f:
        core = f.read()
        files.append(core)
    with open('/etc/hadoop/conf.empty/hdfs-site.xml', 'r') as f:
        hdfs = f.read()
        files.append(hdfs)
    with open('/etc/hadoop/conf.empty/mapred-site.xml', 'r') as f:
        mapred = f.read()
        files.append(mapred)
    with open('/etc/hadoop/conf.empty/yarn-site.xml', 'r') as f:
        yarn = f.read()
        files.append(yarn)

    status_set('active', 'Ready to provide Hadoop XML files.')

    config = endpoint_from_flag('endpoint.config.joined')
    config.provide_hadoop_files(files)


@when('druid-config.installed', 'hadoop.hdfs.ready')
@when_not('druid-config.hdfs_dirs_created')
def create_druid_directories(hadoop):
    """
    Creates the necessary directories in HDFS for Druid to function.

    Could incorporate this into a shell script at some point.
    """
    log('Setting up HDFS directories, cwd: ' + os.getcwd())
    check_call('./scripts/hdfs_setup.sh')
    log('Setting up HDFS directories successful.')
    set_flag('druid-config.hdfs_dirs_created')


@when('druid-config.installed', 'zookeeper.ready', 'mysql.available', 'endpoint.config.joined')
@when_not('hadoop.hdfs.ready')
def write_config_file(zookeeper, mysql, endpoint):
    """
    Writes common config file once Zookeeper and MySQL are joined. This method should be triggered whenever a new
    Druid charm is connected.
    """

    status_set('maintenance', 'Preparing Config file')
    zk = configure_zookeepers(zookeeper)
    jdbc = create_mysql_url(mysql)
    context = {
        'zookeeper_ip': zk,
        'mysql_connectURI': jdbc,
        'mysql_user': mysql.user(),
        'mysql_password': mysql.password()
    }

    # If any changes are made to S3 configuration settings, write an S3 config file. Else, write a local storage
    #  config file.
    if (data_changed('s3-storage', hookenv.config('s3-storage'))
        or data_changed('s3-bucket', hookenv.config('s3-bucket'))
        or data_changed('s3-accessKey', hookenv.config('s3-accessKey'))
        or data_changed('s3-secretKey', hookenv.config('s3-secretKey')) )\
        and hookenv.config('s3-storage'):

            log('S3 settings detected. Configuring for S3...')
            context['s3_bucket'] = hookenv.config('s3-bucket')
            context['s3_access_key'] = hookenv.config('s3-accessKey')
            context['s3_secret_key'] = hookenv.config('s3-secretKey')

            config_file = render('s3.common.runtime.properties', None, context)
    else:
        log('No S3 settings detected. Configuring for local storage...')
        config_file = render('common.runtime.properties', None, context)

    set_flag('druid-config.configured')
    status_set('active', 'Ready to provide common config file.')

    config = endpoint_from_flag('endpoint.config.joined')
    config.provide_config(config_file)

@when('druid-config.installed', 'zookeeper.ready', 'mysql.available', 'endpoint.config.joined', 'hadoop.hdfs.ready')
def write_config_file_hdfs(zookeeper, mysql, endpoint, hadoop):
    """
       Writes common config file once Zookeeper, MySQL and Hadoop-Plugin are joined. This method should be triggered
       whenever a new Druid charm is connected.
    """

    status_set('maintenance', 'Preparing Config file')
    zk = configure_zookeepers(zookeeper)
    jdbc = create_mysql_url(mysql)
    context = {
        'zookeeper_ip': zk,
        'mysql_connectURI': jdbc,
        'mysql_user': mysql.user(),
        'mysql_password': mysql.password()
    }

    log('HDFS relation found. Configuring for HDFS...')
    config_file = render('hdfs.common.runtime.properties', None, context)

    set_flag('druid-config.configured')
    status_set('active', 'Ready to provide common config file.')

    config = endpoint_from_flag('endpoint.config.joined')
    config.provide_config(config_file)

def configure_zookeepers(zookeeper):
    """
        Check for new Zookeeper nodes, add them to list of zookeepers.

    :param zookeeper:
    :return:
    """
    zks = zookeeper.zookeepers()
    zklist = ''
    for zk_unit in zookeeper.zookeepers():
        zklist += add_zookeeper(zk_unit['host'], zk_unit['port'])
    zklist = zklist[:-1]
    return zklist


def add_zookeeper(host, port):
    """
        Return a ZK hostline for the config.
    """
    return host+':'+port+','


def create_mysql_url(mysql):
    return 'jdbc:mysql://'+mysql.host()+':'+str(mysql.port())+'/'+mysql.database()

