#!/bin/bash

su hdfs -c "hdfs dfs -mkdir /user/root/quickstart"
su hdfs -c "hdfs dfs -mkdir /druid"
su hdfs -c "hdfs dfs -mkdir /druid/segments"
su hdfs -c "hdfs dfs -mkdir /druid/indexing-logs"
su hdfs -c "hdfs dfs -chmod 777 /druid"
su hdfs -c "hdfs dfs -chmod 777 /druid/segments"
su hdfs -c "hdfs dfs -chmod 777 /druid/indexing-logs"
su hdfs -c "hdfs dfs -chmod -R 777 /tmp"
su hdfs -c "hdfs dfs -chmod -R 777 /user"